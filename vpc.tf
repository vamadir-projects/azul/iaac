provider "aws" {
  region = "eu-central-1"
}

variable vpc_cidr_block {}
variable private_subnet_cidr_blocks {}
variable public_subnet_cidr_blocks {}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones
data "aws_availability_zones" "azs" {}

# For VPC
# https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest
module "weather-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.5.2"

  name = "weather-vpc"
  cidr = var.vpc_cidr_block
  private_subnets = var.private_subnet_cidr_blocks
  public_subnets = var.public_subnet_cidr_blocks
# https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest?tab=inputs
  azs = data.aws_availability_zones.azs.names

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/weather-eks-cluster" = "shared"
  }

# Cloud subnet
  public_subnet_tags = {
    "kubernetes.io/cluster/weather-eks-cluster" = "shared"
# Cloud balancer
    "kubernetes.io/role/elb" = 1
  }

# K8 subnet
  private_subnet_tags = {
    "kubernetes.io/cluster/weather-eks-cluster" = "shared"
# Kubernetes Balancer
    "kubernetes.io/role/internal-elb" = 1
  }
}
