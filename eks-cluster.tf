# For K8
# https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "20.2.1"

  cluster_name = "weather-eks-cluster"
  cluster_version = "1.29"
  cluster_endpoint_public_access  = true

  # Connect subnet by module VPC
  subnet_ids = module.weather-vpc.private_subnets
  vpc_id = module.weather-vpc.vpc_id

  tags = {
    environment = "development"
    application = "weather"
  }

  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.small"]
    }
  }
  
  # To add the current caller identity as an administrator
  enable_cluster_creator_admin_permissions = true

}

