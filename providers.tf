terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.37.0"
    }
  }
  backend "s3" {
    bucket = "weather-terraform-state"
    key    = "terraform.tfstate"
    region = "eu-central-1"
  }
}